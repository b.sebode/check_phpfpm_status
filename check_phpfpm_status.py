#!/usr/bin/env python3
# -*- coding: utf-8 -*-

from optparse import OptionParser
from urllib.request import urlopen
from json import loads
from sys import exit

parser = OptionParser('Usage: %prog [-H HOSTNAME] [-p PORT] [-u URL] [-w WARNING] [-c CRITICAL] [-m MAXWARN] [-v]', version='%prog 1.0')
parser.add_option('-H', type='string', dest='hostname', default='localhost', help='Hostname (Default: localhost)')
parser.add_option('-p', type='int', dest='port', default='80', help='Port (Default: 80)')
parser.add_option('-u', type='string', dest='url', default='/status', help='Status URL (Default: /status)')
parser.add_option('-w', type='int', dest='warning', default=None, help='Warning if more than x active processes (Default: None)')
parser.add_option('-c', type='int', dest='critical', default=None, help='Critical if more than x active processes (Default: None)')
parser.add_option('-m', type='int', dest='maxwarn', default=None, help='Warning if maximum was reached more than x times (Default: None)')
parser.add_option('-v', action='store_true', dest='debug', help='Print all fetched data')
(args, options) = parser.parse_args()

status = {
    0: 'OK',
    1: 'WARNING',
    2: 'CRITICAL',
    3: 'UNKNOWN',
}
exitcode=0
debug=''

def output(exitcode,msg='',perfdata='',debug=''):
    if debug != '': perfdata = perfdata+'\n'
    print ("{}: {}|{}{}".format(status[exitcode],msg,perfdata,debug))
    exit(exitcode)

url = "http://{}:{}{}?json".format(args.hostname, args.port, args.url)
try:
    response = urlopen(url)
except:
    exitcode = 2
    output(exitcode,'Could not fetch {}'.format(url))

try:
    json = loads(response.read().decode())
except:
    exitcode = 2
    output(exitcode,'Output of {} is not JSON'.format(url))

json['average'] = round(json['accepted conn']/json['start since'],2)
msg = '{} connections in {}s (average of {}/s), {}/{} processes idle, maximum active processes was {}'.format(
        json['accepted conn'],
        json['start since'],
        json['average'],
        json['idle processes'],
        json['total processes'],
        json['max active processes'],
    )
perfdata = "'IDLE'={};'ACTIVE'={};{};{};'CONN/S'={};".format(
        json['idle processes'],
        json['active processes'],
        args.warning,
        args.critical,
        json['average'],
    )
if args.warning != None and json['active processes'] >= args.warning:
    exitcode = 1
if json['max children reached'] > 0:
    msg = 'Maximum has been reached {} times '.format(json['max children reached']) + msg
    if args.maxwarn != None and json['max children reached'] >= args.maxwarn:
        exitcode = 1
if args.critical != None and json['active processes'] >= args.critical:
    exitcode = 2
if json['idle processes'] < 1:
    exitcode = 2
if args.debug: debug = json

output(exitcode,msg,perfdata,debug)